/**
 * This simple G Suite add-on shows a random image of a cat in the sidebar. When
 * opened manually (the homepage card), some static text is overlayed on
 * the image, but when contextual cards are opened a new cat image is shown
 * with the text taken from that context (such as a message's subject line)
 * overlaying the image. There is also a button that updates the card with a
 * new random cat image.
 *
 * Click "File > Make a copy..." to copy the script, and "Publish > Deploy from
 * manifest > Install add-on" to install it.
 */

/**
 * The maximum number of characters that can fit in the cat image.
 */
var MAX_MESSAGE_LENGTH = 40;


// var CLIENT_ID = '866138300550-ursie00busjnk748jgh8o98lsloj1hfp.apps.googleusercontent.com';
// var CLIENT_SECRET = 'jYm2WZT0L_iDHayDmnQUCN5b';

function onHomepage(e) {
//  console.log('Redirect url ',OAuth2.getRedirectUri('1GXt7BPAmVR0ZagK6JYo7S2rpL5CUL5Wy6MGM49kWx0zkvtK-6gnlc649'));
  
//  console.log('Auth Mode from event', e.authMode);
//  console.log('Auth Info ',ScriptApp.getAuthorizationInfo(ScriptApp.AuthMode.FULL));
//  console.log('Auth Url,', ScriptApp.getAuthorizationInfo(ScriptApp.AuthMode.FULL).getAuthorizationUrl());
//  OAuth2.getRedirectUri(optScriptId)
  // var isAuthenticated = true;
  
  console.log('OAuth Token',ScriptApp.getOAuthToken());

  var emailId = CalendarApp.getId();
  var formData = {
    'email': emailId
  };
  var options = {
    'method' : 'post',
    'payload' : formData
  };
  var userDetails = UrlFetchApp.fetch("https://dev-google-addin-ss.emvigotechnologies.com/user/activateApps",options);
  var finalData = JSON.parse(userDetails);
  if(finalData.access) {
    return createDashboard(e);
  } else {
    return createActivatePage();
  }
  // if(isAuthenticated) {
  //   var userEmail = Session.getActiveUser().getEmail();
    
  //   if(userDetails == null || userDetails == undefined) {
  //     return createDashboard('SM User', 0, 0);
  //   }
  //   else return createDashboard();
  // }
  
  // else return createActivatePage();
  
  
  
}


/**
 * Creates a card with an image of a cat, overlayed with the text.
 * @param {String} text The text to overlay on the image.
 * @param {Boolean} isHomepage True if the card created here is a homepage;
 *      false otherwise. Defaults to false.
 * @return {CardService.Card} The assembled card.
 */
function createDashboard(e,username, treesPlanted, meetingsCount) {
    
//  var image = CardService.newImage().setImageUrl('https://firebasestorage.googleapis.com/v0/b/nikki-rafiki-94345.appspot.com/o/Selection_013.jpg?alt=media&token=6c41fcfd-aa6a-45b5-ad4d-df4d65856de3');  
//  var imageMeetings = CardService.newImage().setImageUrl("https://firebasestorage.googleapis.com/v0/b/nikki-rafiki-94345.appspot.com/o/Selection_014%20(1).jpg?alt=media&token=c4cd54d4-4f30-4ca4-9fc5-4050efb1ffc3");
  var countsImage = CardService.newImage().setImageUrl("https://dev-google-addin-ss.emvigotechnologies.com/meetings/"+ 636);
  var logoUrl = CardService.newImage().setImageUrl("https://firebasestorage.googleapis.com/v0/b/nikki-rafiki-94345.appspot.com/o/Selection_016.png?alt=media&token=2df95d8e-a0f1-46ff-9b9c-9b95f1008db3");
  var blankSectionUrl = CardService.newImage().setImageUrl("https://firebasestorage.googleapis.com/v0/b/nikki-rafiki-94345.appspot.com/o/nlank.png?alt=media&token=f3e77dbf-6ef8-4769-b7cd-a59f2e9db821");
                                                           
  var whitespaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  var rightWhitespaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  var button = CardService.newTextButton().setText(whitespaces+ 'Go To Dashboard' + rightWhitespaces).setTextButtonStyle(CardService.TextButtonStyle.FILLED).setOpenLink(CardService.newOpenLink().setUrl("https://www.google.com"));
  
  var blankSection = CardService.newCardSection().addWidget(blankSectionUrl);
  
  var buttonSection = CardService.newCardSection().addWidget(button);
//  var sectionTreesPlanted = CardService.newCardSection().addWidget(image);
//  var sectionMeetingsAttended = CardService.newCardSection().addWidget(imageMeetings);
  var countsSection = CardService.newCardSection().addWidget(countsImage);
  var logoSection = CardService.newCardSection().addWidget(logoUrl);

  var toggleVal = false;
  if(e.calendar.attendees) {
    var atList = e.calendar.attendees;

    var valExist = atList.filter(function (item) {
      return item.email == "user1@example.com";
    });
    if(valExist) {
      toggleVal = true;
    } else {
      toggleVal = false;
    }
  }

  var smSwitch = CardService.newSwitch().setFieldName("form_input_switch_key").setValue("form_input_switch_value").setOnChangeAction(CardService.newAction().setFunctionName("optOutOption")).setSelected(toggleVal);
  var switchKeyValue = CardService.newKeyValue().setTopLabel("Opt-Out this meeting from a").setContent("Sustainable Meeting").setSwitch(smSwitch);
  var switchSection = CardService.newCardSection().addWidget(switchKeyValue);
  
  var card;
  
  if(e.calendar.capabilities) {
    card = CardService.newCardBuilder()
  .addSection(buttonSection)
//  .addSection(blankSection)
//  .addSection(sectionTreesPlanted)
//  .addSection(blankSection)
//  .addSection(sectionMeetingsAttended)
  .addSection(countsSection)
  .addSection(logoSection)
  .addSection(switchSection);
  } else {
     card = CardService.newCardBuilder()
  .addSection(buttonSection)
//  .addSection(blankSection)
//  .addSection(sectionTreesPlanted)
//  .addSection(blankSection)
//  .addSection(sectionMeetingsAttended)
  .addSection(countsSection)
  .addSection(logoSection);
  }

  return card.setHeader(CardService.newCardHeader().setTitle("Hi Akshat Bais").setSubtitle('Online')).build();
  
}

function createActivatePage() {
  
  var logoUrl = CardService.newImage().setImageUrl("https://firebasestorage.googleapis.com/v0/b/nikki-rafiki-94345.appspot.com/o/Selection_016.png?alt=media&token=2df95d8e-a0f1-46ff-9b9c-9b95f1008db3");
  var blankSectionLogo = CardService.newImage().setImageUrl("https://firebasestorage.googleapis.com/v0/b/nikki-rafiki-94345.appspot.com/o/blankSection.png?alt=media&token=d1bbe1c7-e61b-40b2-857b-2521a49213aa");
  
  var whitespaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  var rightWhitespaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  
  var authButtonAction = CardService.newAction()
      .setFunctionName('serviceOAuthInit');
  var authBtn = CardService.newTextButton()
      .setText(whitespaces+ '             Activate             ' + whitespaces)
      .setOnClickAction(authButtonAction)
      .setTextButtonStyle(CardService.TextButtonStyle.FILLED);
  var authButtonVal = CardService.newButtonSet()
      .addButton(authBtn);

  // var button = CardService.newTextButton().setText(whitespaces+ '             Activate             ' + whitespaces).setTextButtonStyle(CardService.TextButtonStyle.FILLED).setFunctionName('serviceOAuthInit');
  
  var blankSection = CardService.newCardSection().addWidget(blankSectionLogo);
  var buttonSection = CardService.newCardSection().addWidget(authButtonVal);
  var logoSection = CardService.newCardSection().addWidget(logoUrl);
  
  var card = CardService.newCardBuilder().addSection(blankSection).addSection(logoSection).addSection(buttonSection);
  
  return card.build();
  
}

// opt-out functionality 

function optOutOption(e) {
  if(e.calendar.capabilities && e.formInput.form_input_switch_key) {
  var calendarEventActionResponse = CardService.newCalendarEventActionResponseBuilder()
    .addAttendees(["user1@example.com"]);
  
  //console.log(Calendar.Events.get(e.calendar.calendarId, e.calendar.id).attendees);  
  return calendarEventActionResponse.build();
  }
}


//OAuth for third party starts here

function serviceOAuthInit() {
  
  var service = getOAuthService();
  
   CardService.newAuthorizationException()
  .setAuthorizationUrl(service.getAuthorizationUrl())
  .setResourceDisplayName("Display name to show to the user")
  .setCustomUiCallback('create3PAuthorizationUi')
  .throwException();

}

// Creating the 3P auth card

function create3PAuthorizationUi() {

  var service = getOAuthService();
  var authUrl = service.getAuthorizationUrl();
  
  var maybeAuthorized = service.hasAccess();
  

  
  var authButton = CardService.newTextButton()
      .setText('Begin Authorization')
      .setAuthorizationAction(CardService.newAuthorizationAction()
      .setAuthorizationUrl(authUrl));

  var promptText =
      'To show you information from your 3P account that is relevant' +
      ' to the recipients of the email, this add-on needs authorization' +
      ' to: <ul><li>Read recipients of the email</li>' +
      '         <li>Read contact information from 3P account</li></ul>.';

  var card = CardService.newCardBuilder()
      .setHeader(CardService.newCardHeader()
          .setTitle('Authorization Required'))
      .addSection(CardService.newCardSection()
          .setHeader('This add-on needs access to your 3P account.')
          .addWidget(CardService.newTextParagraph()
              .setText(promptText))
          .addWidget(CardService.newButtonSet()
              .addButton(authButton)))
      .build();
  return [card];
}


function getOAuthService() {
  return OAuth2.createService('calendar')
      .setAuthorizationBaseUrl('https://accounts.google.com/o/oauth2/auth')
      .setTokenUrl('https://accounts.google.com/o/oauth2/token')
      .setClientId('985040735401-p8eimmdbjr5fadjp1tsg7ghuq1kpndcp.apps.googleusercontent.com')
      .setClientSecret('us3TwNzT91x6BGudFIicnM6g')
      .setCallbackFunction('authCallback')
      .setScope('https://www.googleapis.com/auth/calendar.addons.execute https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.addons.current.event.read https://www.googleapis.com/auth/calendar.addons.current.event.write https://www.google.com/calendar/feeds https://www.googleapis.com/auth/calendar.readonly https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar.events.readonly https://www.googleapis.com/auth/calendar.settings.readonly https://www.googleapis.com/auth/script.locale https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/script.external_request https://www.googleapis.com/auth/userinfo.email')
      .setParam('access_type', 'offline')
      .setParam('prompt', 'consent');
}


// it will callafter the authorization

function authCallback(callbackRequest) {
 var authorized = getOAuthService().handleCallback(callbackRequest);
 if (authorized) {
   return HtmlService.createHtmlOutput(
     'Success! <script>setTimeout(function() { top.window.close() }, 1);</script>');
 } else {
   return HtmlService.createHtmlOutput('Denied');
 }
}


